<?php 

/*
Template name: Frontpage Template
*/

get_header(); ?>

<!-- Start Header -->
<header 
class="header creative position-relative bg-img height-100-vh valign" 
data-scroll-index="0" 
data-overlay-dark="6" 
data-background="<?php header_image(); ?>" 
data-stellar-background-ratio="0.5">

<?php get_template_part('menu_section'); ?>

<!-- Start Banner Container -->
<div class="container">
    <div class="row">
        <div class="col-12 text-center caption">
            <h1 class="banner-headline clip no-margin">
                <span class="blc">Somos </span>
                <span class="banner-words-wrapper">
                    <b class="is-visible font-weight-600">Criativos</b>
                    <b class="font-weight-600">Ousados</b>
                    <b class="font-weight-600">a solução</b>
                </span>
            </h1>
            <p class="margin-30px-bottom sm-margin-20px-bottom xs-display-none">Fornecemos o melhor para o nosso cliente e respeitamos a ideia de design de negócios.</p>
            <div class="social-links xs-margin-20px-top">
            <?php if (is_active_sidebar('sidebar-header')) : ?>
            <?php dynamic_sidebar('sidebar-header'); ?>
            <?php endif; ?>                
            </div>
        </div>
    </div>
</div>
<!-- End Banner Container -->

<div class="svg">
    <svg fill="#fff" version="1.1" width="100%" height="100" viewBox="0 0 100 102" preserveAspectRatio="none">
        <path d="M0 30 L50 90 L100 30 V100 H0"></path>
    </svg>
</div>

</header>
<!-- End Header -->

<!-- Start About Section-->
<section data-scroll-index="1" class="padding-100px-top padding-130px-bottom xs-padding-80px-top xs-padding-100px-bottom">
    <?php 
        query_posts('post_type=page&name="sobre-nos"');
        if(have_posts()): while(have_posts()): the_post();
    ?>
    <div class="container">
        <div class="row">
            <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
                <h2 class="font-size34 md-font-size30 sm-font-size26 xs-font-size24 font-weight-700 section-title style2"><?php the_title(); ?></h2>
            </div>
        </div>
        <div class="row">
            <!-- start about me section-->
            <div class="col-md-5 col-sm-12 sm-margin-30px-bottom">                
                <div class="shadow porfile-photo">
                    <?php the_post_thumbnail(); ?>                    
                </div>
            </div>
            <!-- end about me section-->

            <!-- start skill section-->
            <div class="col-md-7 col-sm-12 padding-70px-left md-padding-50px-left xs-padding-15px-left">
                <div class="padding-20px-left sm-no-padding">
                    <h4 class="margin-5px-bottom alt-font font-size26 md-font-size24 xs-font-size20"><?php echo get_post_meta(get_the_id(), 'subtitulo', true); ?></h4>
                    <span class="margin-20px-bottom display-block"><?php echo get_post_meta(get_the_id(), 'resumo', true); ?></span>                    
                    <p class="font-size15 xs-font-size14">
                        <?php the_content() ?>
                    </p>
                </div>
            </div>
            <!-- end skill section-->
        </div>        
    </div>

    <div class="svg gray">
        <svg fill="#f7f7f7" version="1.1" width="100%" height="100" viewBox="0 0 100 102" preserveAspectRatio="none">
            <path d="M0 30 L50 90 L100 30 V100 H0"></path>
        </svg>
    </div>
    <?php endwhile; endif; wp_reset_query(); ?>
</section>
<!-- End About Section-->

<!--  Start Services Section -->
<section class="services bg-light-gray padding-100px-top padding-130px-bottom xs-padding-80px-top xs-padding-100px-bottom" data-scroll-index="2">
    <div class="container">
        <div class="row">
            <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
                <h3 class="font-weight-700 font-size32 md-font-size27 sm-font-size24 xs-font-size20 section-title style2">Nossos serviços</h3>
            </div>
        </div>
        <div class="row">
            <!-- start services item -->
            <div class="col-lg-4 col-md-6">
                <div class="text-center padding-40px-bottom sm-padding-30px-bottom xs-padding-20px-bottom">
                    <div class="display-inline-block margin-20px-bottom xs-margin-15px-bottom"><i class="fas fa-sticky-note text-extra-dark-gray fa-4x"></i></div>
                    <div class="alt-font text-extra-dark-gray margin-10px-bottom font-size18 xs-font-size16">Impressão de lonas e adesivos</div>
                    <p class="width-85 md-width-90 xs-width-100 center-col no-margin-bottom">This creative template design provide ideal and variety of design services and solutions for website.</p>
                </div>
            </div>
            <!-- end services item -->

            <!-- start services item -->
            <div class="col-lg-4 col-md-6">
                <div class="text-center padding-40px-bottom sm-padding-30px-bottom xs-padding-20px-bottom">
                    <div class="display-inline-block margin-20px-bottom xs-margin-15px-bottom"><i class="fas fa-home text-extra-dark-gray fa-4x"></i></div>
                    <div class="alt-font text-extra-dark-gray margin-10px-bottom font-size18 xs-font-size16">Fachadas em lona e ACM</div>
                    <p class="width-85 md-width-90 xs-width-100 center-col no-margin-bottom">This creative template design provide ideal and variety of design services and solutions for website.</p>
                </div>
            </div>
            <!-- end services item -->

            <!-- start services item -->
            <div class="col-lg-4 col-md-6">
                <div class="text-center padding-40px-bottom sm-padding-30px-bottom xs-padding-20px-bottom">
                    <div class="display-inline-block margin-20px-bottom xs-margin-15px-bottom"><i class="fas fa-font text-extra-dark-gray fa-4x"></i></div>
                    <div class="alt-font text-extra-dark-gray margin-10px-bottom font-size18 xs-font-size16">Letra caixa em alumínio e PVC</div>
                    <p class="width-85 md-width-90 xs-width-100 center-col no-margin-bottom">This creative template design provide ideal and variety of design services and solutions for website.</p>
                </div>
            </div>
            <!-- end services item -->

            <!-- start services item -->
            <div class="col-lg-4 col-md-6">
                <div class="text-center sm-padding-30px-bottom xs-padding-20px-bottom">
                    <div class="display-inline-block margin-20px-bottom xs-margin-15px-bottom"><i class="fas fa-car text-extra-dark-gray fa-4x"></i></div>
                    <div class="alt-font text-extra-dark-gray margin-10px-bottom font-size18 xs-font-size16">Plotagem de veículos</div>
                    <p class="width-85 md-width-90 xs-width-100 center-col no-margin-bottom">This creative template design provide ideal and variety of design services and solutions for website.</p>
                </div>
            </div>
            <!-- end services item -->

            <!-- start services item -->
            <div class="col-lg-4 col-md-6">
                <div class="text-center xs-padding-20px-bottom">
                    <div class="display-inline-block margin-20px-bottom xs-margin-15px-bottom"><i class="fas fa-cut text-extra-dark-gray fa-4x"></i></div>
                    <div class="alt-font text-extra-dark-gray margin-10px-bottom font-size18 xs-font-size16">Cortes a laser em MDF e acrílico</div>
                    <p class="width-85 md-width-90 xs-width-100 center-col no-margin-bottom">This creative template design provide ideal and variety of design services and solutions for website.</p>
                </div>
            </div>
            <!-- end services item -->

            <!-- start services item -->
            <div class="col-lg-4 col-md-6">
                <div class="text-center">
                    <div class="display-inline-block margin-20px-bottom xs-margin-15px-bottom"><i class="fas fa-address-card text-extra-dark-gray fa-4x"></i></div>
                    <div class="alt-font text-extra-dark-gray margin-10px-bottom font-size18 xs-font-size16">Cartões de visita e folders</div>
                    <p class="width-85 md-width-90 xs-width-100 center-col no-margin-bottom">This creative template design provide ideal and variety of design services and solutions website.</p>
                </div>
            </div>
            <!-- end services item -->
        </div>
    </div>

    <div class="svg orange">
        <svg fill="#fff" version="1.1" width="100%" height="100" viewBox="0 0 100 102" preserveAspectRatio="none">
            <path d="M0 30 L50 90 L100 30 V100 H0"></path>
        </svg>
    </div>

</section>
<!-- End Services Section -->

<!-- Start Budget Section -->
<section class="contact bg-black no-padding-bottom padding-130px-top xs-padding-80px-top">
    <div class="container padding-130px-bottom xs-padding-80px-bottom">
        <div class="row">
            <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
                <h3 class="font-weight-700 font-size32 md-font-size27 sm-font-size24 xs-font-size20 section-title style2">Faça um orçamento</h3>
            </div>
        </div>
        <div class="row">
            <!-- start get in touch -->
            <div class="col-lg-12">
                <?= do_shortcode('[contact-form-7 title="Formulário Contato"]') ?>
                
                <div class="wrap-whatsapp text-center">
                    <?php echo do_shortcode("[chat]"); ?>
                </div>
            </div>
            <!-- end get in touch -->
        </div>
    </div>

    <div class="svg">
        <svg fill="#fff" version="1.1" width="100%" height="100" viewBox="0 0 100 102" preserveAspectRatio="none">
            <path d="M0 30 L50 90 L100 30 V100 H0"></path>
        </svg>
    </div>
</section>
<!-- End Budget Section -->

<!-- Start Portfolio Section -->
<section class="portfolio padding-100px-top padding-130px-bottom xs-padding-80px-top xs-padding-100px-bottom" data-scroll-index="3" id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
                <h3 class="font-weight-700 font-size32 md-font-size27 sm-font-size24 xs-font-size20 section-title style2">Nosso Portfólio</h3>
            </div>
        </div>
        <div class="row">
            <!-- Start links -->
            <div class="filtering col-sm-12 text-center">
                <span data-filter='*' class="active">Todas</span>
                <?php
                    $args = [ 'taxonomy' => 'categoria_portfolio', 'hide_empty' => true];
                    $categories = get_terms( $args );
                    if ( ! empty( $categories ) && ! is_wp_error( $categories ) ):
                        foreach($categories as $cat):
                ?>
                <span data-filter='.<?php echo $cat->slug ?>'><?php echo $cat->name ?></span>
                <?php
                        endforeach;
                    endif;
                ?>
            </div>
            <!-- End links -->
            <div class="clearfix"></div>

            <!-- gallery -->
            <div class="gallery text-center width-100">

            <?php
                $args = [ 'post_type' => 'portfolio','posts_per_page' => 60,'orderby' => 'rand' ];
                $loop = new WP_Query( $args );

                if( $loop->have_posts() ):
                    while( $loop->have_posts() ):
                        $loop->the_post();

                        // exib categories
                        $categories = get_the_terms( $post->ID, 'categoria_portfolio' );
                        $classes = '';
                        foreach($categories as $cat) $classes = $classes . ' ' . $cat->slug;
            ?>

            <!-- start gallery item -->
            <div class="col-md-4 items <?php echo $classes ?>">
                    <div class="item-img">
                        <?php the_post_thumbnail('portfolio', array('class' => '')); ?>   
                        <div class="item-img-overlay valign">
                            <div class="overlay-info width-100 vertical-center">
                                <a href="<?php echo the_post_thumbnail_url() ?>" class="popimg">
                                    <i class="fa fa-search-plus"></i>
                                </a>
                                <h6><?php echo the_title(); ?></h6>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end gallery item -->

            <?php
                    endwhile;
                    wp_reset_postdata();

                endif;
            ?>

                <div class="clear-fix"></div>

            </div>
        </div>
    </div>

</section>
<!-- End Portfolio Section -->

<!-- Start Contact Section -->
<section class="contact bg-light-gray no-padding-bottom no-padding-top" data-scroll-index="4">

    <div class="bg-white padding-30px-tb border-top border-color-medium-gray">
        <div class="container">
            <div class="row info">

                <!-- start quickly contact -->

                <div class="col-lg-4 col-md-3 item text-center border-right xs-no-border-right xs-border-bottom border-color-medium-gray xs-margin-20px-bottom xs-padding-20px-bottom">
                    <span class="icon font-size32"><i class="icon-phone"></i></span>
                    <div class="cont">
                        <h6>Telefone</h6>
                        <p><i class="fab fa-whatsapp" style="color: #16b923;"></i> +55 67 99219-9891 | 67 98433-4364<br>67 99172-4798<br>
                        <i class="fas fa-phone"></i> 67 3431-4364</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 item text-center border-right xs-no-border-right xs-border-bottom border-color-medium-gray xs-margin-20px-bottom xs-padding-20px-bottom">
                    <span class="icon font-size32"><i class="icon-map"></i></span>
                    <div class="cont">
                        <h6>Endereço</h6>
                        <p>Rua Duque de Caxias, n° 249 - Ponta Porã</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 item text-center">
                    <span class="icon font-size32"><i class="icon-envelope"></i></span>
                    <div class="cont">
                        <h6>E-mail</h6>
                        <p>agiliza.xp@msn.com</p>
                    </div>
                </div>

            </div>
            <!-- end quickly contact -->
        </div>
    </div>

</section>
<!-- End Contact Section -->

<?php get_footer(); ?>
