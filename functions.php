<?php

// CPTS
// require get_template_directory() . '/inc/cpts.php';
// Requerendo os Term Metas
// require get_template_directory() . '/inc/terms-meta.php';
// Requerendo o arquivo do Customizer
// require get_template_directory() . '/inc/customizer.php';
// Requerendo o arquivo de AJAX
// require get_template_directory() . '/inc/ajax-functions.php';

define('WA_FUNCTIONS', get_template_directory()  . '/includes');
define('WA_INDEX_JS', get_template_directory_uri()  . '/assets/js');
define('WA_INDEX_CSS', get_template_directory_uri()  . '/assets/css');

//Remove pages for non-admins
add_action('admin_menu', 'adjust_admin_menu_nonadm', 999);

function adjust_admin_menu_nonadm()
{
    if (!current_user_can('manage_options')) {

        // get the the role object
        $editor = get_role('editor');
        // add $cap capability to this role object
        $editor->add_cap('edit_theme_options');
        if (class_exists('Contact')) {
            remove_menu_page('contact');
        }
        remove_menu_page('tools.php'); // Tools
        //remove_menu_page('edit.php'); // Posts
        //remove_menu_page('edit.php?post_type=page'); // Pages
        remove_menu_page('options-general.php'); // Settings
        remove_menu_page('edit-comments.php'); // Comments
        remove_menu_page('wpcf7'); // CONTACT FORM
        remove_submenu_page('themes.php', 'widgets.php'); // widgets
        remove_submenu_page('themes.php', 'themes.php'); // edit theme
    }
}

// Estização de página de login
function login_styles()
{ ?>
    <style type="text/css">
        .login h1 a {
            background-size: 250px 150px;
            width: 250px;
            height: 150px;
            background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/logo-morfose.png')
        }
    </style>
<?php }
add_action('login_head', 'login_styles');

// Link logo login
function my_login_logo_url()
{
    return get_bloginfo('url');
}
add_filter('login_headerurl', 'my_login_logo_url');

// Mudar nome ao passar o mouse
function my_login_logo_url_title()
{
    return 'Agiliza MS';
}
add_filter('login_headertitle', 'my_login_logo_url_title');

function contact_message()
{
    return '<p style="text-align: center"><strong>Contato:</strong><br>Mateus 67 99615-3429</p>';
}
add_filter('login_message', 'contact_message');

// adicionando classes na li dos menus
function add_additional_class_on_li($classes, $item, $args)
{
    if ($args->add_li_class) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);

// removendo barra de admin
function remove_admin_bar()
{
    //show_admin_bar(false);
}
add_action('after_setup_theme', 'remove_admin_bar');

// Carregando css e js
function load_scripts()
{
    /*
  * CSS
  */
    wp_register_style('plugins', WA_INDEX_CSS . '/plugins.css', array(), '1', 'all');
    wp_register_style('base', WA_INDEX_CSS . '/style.css', array(), '1', 'all');

    wp_enqueue_style('plugins');
    wp_enqueue_style('base');
    wp_enqueue_style('style', get_stylesheet_uri(), array(), '1', 'all');

    /*
  * Java script
  */
    //wp_enqueue_script('jquery');
    wp_register_script('core', WA_INDEX_JS . '/core.min.js', array(),  TRUE);
    wp_register_script('scripts', WA_INDEX_JS . '/scripts.js', array(),  true);

    wp_enqueue_script('core');
    wp_enqueue_script('scripts');
}
add_action('wp_enqueue_scripts', 'load_scripts');

function walder_config()
{
    // Registrando nossos menus
    register_nav_menus(
        [
            'my_main_menu' => 'Menu Principal'
        ]
    );

    add_theme_support('custom-header');
    add_theme_support('post-thumbnails');
    add_theme_support('post-formats', ['video', 'image']);
    add_theme_support('title-tag');
    add_theme_support('custom-logo');

    // tamanhos Gutenberg
    add_theme_support('align-wide');

    add_image_size('500x', 500, 500, true);
    add_image_size('portfolio', 800, 800, true);
}
add_action('after_setup_theme', 'walder_config', 0);

//Custom Admin Bar
add_action('wp_before_admin_bar_render', 'wps_admin_bar');

function wps_admin_bar()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('about');
    $wp_admin_bar->remove_menu('wporg');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');
    //$wp_admin_bar->remove_menu('view-site');
    //$wp_admin_bar->remove_menu('edit');
    $wp_admin_bar->remove_menu('new-content');
    $wp_admin_bar->remove_menu('comments');
}
/* Para Paginação funcionar */
function custom_posts_per_page($query)
{

    if ($query->is_post_type_archive('wa_product')) {
        set_query_var('posts_per_page', -1);
    }
}
add_action('pre_get_posts', 'custom_posts_per_page');


// Modificando o get title
function my_theme_archive_title($title)
{
    if (is_category()) {
        $title = single_cat_title('', false);
    } elseif (is_tag()) {
        $title = single_tag_title('', false);
    } elseif (is_author()) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif (is_post_type_archive()) {
        $title = post_type_archive_title('', false);
    } elseif (is_tax()) {
        $title = single_term_title('', false);
    }

    return $title;
}

add_filter('get_the_archive_title', 'walder_archive_title');

add_filter('wp_list_categories', function ($html) {
    return str_replace(' current-cat', ' active', $html);
});

// Registrando Sidebars
add_action('widgets_init', 'walder_sidebars');
function walder_sidebars()
{
  register_sidebar(
    array(
      'name' => 'Header Sidebar',
      'id' => 'sidebar-header',
      'description' => '',
      'before_widget' => '',
      'after_widget' => '',
      'before_title' => '',
      'after_title' => ''
    )
  );
}