<!-- Start Navbar -->
<nav class="navbar navbar-expand-lg border-none nav-transparent">
    <div class="container padding-15px-lr md-padding-15px-lr sm-padding-15px-all">

        <!-- start logo -->
        <?php
        $custom_logo_id = get_theme_mod('custom_logo');
        $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
        if (has_custom_logo()) {
            echo '<a class="logo" rel="home" title="' . esc_attr(get_bloginfo('name')) . '" href="javascript:void(0);" data-scroll-nav="0">
                        <img alt="' . esc_attr(get_bloginfo('name')) . '" title="' . esc_attr(get_bloginfo('name')) . '" src="' . esc_url($logo[0]) . '">
                    </a>';
        } else {
            echo '<a href="' . site_url() . '">
                    <h1>' . esc_attr(get_bloginfo('name')) . '</h1>
                    </a>';
        }
        ?>
        <!-- end Logo -->

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"><i class="fas fa-bars"></i></span>
        </button>

        <!-- navbar links -->
        <div class="collapse navbar-collapse" id="navbarContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="javascript:void(0);" data-scroll-nav="0">Início</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-scroll-nav="1">Sobre nós</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-scroll-nav="2">Serviços</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-scroll-nav="3">Portfólio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-scroll-nav="4">Contato</a>
                </li>
            </ul>
        </div>
        <!-- end navbar links -->
    </div>
</nav>
<!-- End Navbar  -->